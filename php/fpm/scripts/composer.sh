#!/bin/sh

set -euf -o pipefail

# Install composer
curl --silent --show-error -LO https://getcomposer.org/installer \
  && php installer --install-dir=/usr/local/bin --filename=composer \
  && chmod 755 /usr/local/bin/composer
